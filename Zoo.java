import java.awt.Color;
public class Zoo{
	public static void main(String[] args){
		
		System.out.println("=====Singa=====");
		Lion Simba = new Lion();
		Simba.WarnaBulu = new Color(255,255,0);
		Simba.nama = "Simba";
		Simba.usia = 5;
		Simba.bb = 10;
		Simba.cetakInformasi();

		System.out.println("=====Kuda=====");	
		Horse Koboy= new Horse();
		Koboy.WarnaBulu = new Color(255,250,250);
		Koboy.nama = "Koboy";
		Koboy.usia = 5;
		Koboy.bb = 100;
		Koboy.cetakInformasi();

		System.out.println("=====Kangguru=====");
		Kangoroo Gobag= new Kangoroo();
		Gobag.WarnaBulu = new Color(255,193,37);
		Gobag.nama = "Gobag";
		Gobag.usia = 8;
		Gobag.bb = 108;
		Gobag.cetakInformasi();
	}
}