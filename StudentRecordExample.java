public class StudentRecordExample{
	public static void main( String [] args){
		//membuat 3 object record
		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();

		//memberi nama siswa
		annaRecord.setName("Anna");
		beahRecord.setName("Beah");
		crisRecord.setName("Cris");
	
		//menampilkan nama siswa anna
		System.out.println(annaRecord.getName());

		//menampilkan jumlah siswa
		System.out.println("Count="+ StudentRecord.getStudentCount());
	}
}